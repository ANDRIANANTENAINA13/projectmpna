#include <math.h>
#include "matrix.h"


//read data matrix
matrix_t *read_data(char *fname)
{
  FILE *f=fopen(fname, "r+");

  if(fname == NULL)
  {
    printf("Error read file\n");
    exit(0);
  }
  int a,b, size;
  double val;
  matrix_t *m;

  rewind(f);
  fscanf(f,"%d %d",&a,&b);
  size = a*b;
  m=NULL;
  m=(matrix_t*)malloc(sizeof(matrix_t)*size);
  //recover different value matrix
  m->row = a;
  m->col = b;
  m->msize = m->row * m->col;
  m->value = (double*)malloc(sizeof(double)*m->msize);
  for (int i = 0; i < m->msize; i++)
  {
      fscanf(f,"%lf",&val);
      m->value[i]=val;
  }
  fclose(f);

  return m;
}

//print matrix
void print_matrix(matrix_t *m)
{
  printf("Row : %d; Column :%d \n", m->row,m->col);
  for (int i = 0; i < m->row; i++)
  {
    for (int j = 0; j < m->col; j++)
    {
      printf("%lf ",m->value[i]);
    }
    printf("\n");
  }
}

//Product of matrix
double dotProduct(matrix_t *m, matrix_t *n)
{
  double mu =0;
  //check row for 2 matrix
  if (m->row != n->row)
  {
    printf("Error matrix have different row\n");
    exit(0);
  }
  //check column for 2 matrix
  if (m->col != n->col)
  {
    printf("Error matrix have different column\n");
    exit(0);
  }

  //product of 2 matrix
  for (int i = 0; i < m->msize; i++)
  {
    mu += m->value[i]*n->value[i];
  }

  return mu;
}

//Proctuct matrix vector
matrix_t *vectProduct(matrix_t *m, matrix_t *v)
{
  matrix_t *res;
  int id;
  //check the column of vector
  if (v->col != 1)
   {
    printf("Error this is sumt vector\n");
    exit(0);
   }
   //check row of vector and matrix
   if (m->row != v->row)
   {
     printf("Error row different for matrix and vector\n");
     exit(0);
   }
   res=(matrix_t*)malloc(sizeof(matrix_t));
   res->row = v->row;
   res->col = 1;
   res->value = (double*)malloc(sizeof(double)*v->row);
   id = m->col;

   for (int i = 0; i < v->row; i++)
     res->value[i] = 0.0;

   for (int i = 0; i < m->col; i++)
   {
     for (int j = 0; j < m->row; j++)
     {
       res->value[i] += m->value[j*id+i]*v->value[i];
     }
   }
   return res;
}

//sumrm vector
double norm_vector(matrix_t *v)
{
  double norm = 0.0, tmp = 0.0;

  //call dot product
  tmp = dotProduct(v,v);
  tmp = abs(tmp);
  norm = sqrt(tmp);
  return norm;
}

//sumrm frobenius
double norm_frobenius(matrix_t *m)
{
  double norm =0.0;
  for (int i = 0; i < m->msize; i++)
  {
    norm += m->value[i] * m->value[i];
  }
  return norm;
}

//addition of matrix
matrix_t *add_v_matrix(matrix_t *m, matrix_t *v)
{
  matrix_t *res = (matrix_t*)malloc(sizeof(matrix_t));
  res->row = m->row;
  res->col = 1;
  res->value = (double*)malloc(sizeof(double)*res->row);

  for (int i = 0; i < res->row; i++)
  {
    res->value[i] = m->value[i] + v->value[i];
  }

  return res;
}
//Horner scheme compute
matrix_t *Horner_compute(matrix_t *m, matrix_t *v)
{
  matrix_t *tmp1 = vectProduct(m,v);
  matrix_t *tmp2 = add_v_matrix(tmp1,v);
  matrix_t *tmp = vectProduct(m,tmp2);
  matrix_t *res = add_v_matrix(tmp, v);

  destroy_alloc(tmp1);
  destroy_alloc(tmp2);
  destroy_alloc(tmp);
  return res;
}

//compute vector
matrix_t *norm_vector_compute( matrix_t *m)
{
  matrix_t *qm = (matrix_t*)malloc(sizeof(matrix_t));
  qm->row = m->row;
  qm->col = 1;
  qm->value = (double*)malloc(sizeof(double)*m->row);
  //compute sumrm matrix
  double norm = norm_vector(m);
  //compute matrix qm
  for (int i = 0; i < m->row; i++)
  {
    qm->value[i] = m->value[i]/norm;
  }

  return qm;
}

double projection(matrix_t *v1, matrix_t *v2)
{
  double res = 0.0;

  res = dotProduct(v1,v2)/dotProduct(v2,v2);

  return res;
}

matrix_t *gram_schmidt(matrix_t *m)
{
  //vector courant
  matrix_t *v=(matrix_t*)malloc(sizeof(matrix_t));
  v->row = m->row;
  v->col = 1;
  v->value = (double*)malloc(sizeof(double)*v->row);

  //vector solution
  matrix_t *vs =(matrix_t*)malloc(sizeof(matrix_t));
  vs->row = m->row;
  vs->col = 1;
  vs->value = (double*)malloc(sizeof(double)*vs->row);

  //acces for iteration
  matrix_t *tmp =(matrix_t*)malloc(sizeof(matrix_t));
  tmp->row = m->row;
  tmp->col = 1;
  tmp->value = (double*)malloc(sizeof(double)*tmp->row);

  //composant non orthogonaux
  matrix_t *v_no = (matrix_t*)malloc(sizeof(matrix_t));
  v_no->row = m->row;
  v_no->col = 1;
  v_no->value = (double*)malloc(sizeof(matrix_t)*v->row);

  //matrix solution
  matrix_t *res = (matrix_t*)malloc(sizeof(matrix_t));
  res->row = m->row;
  res->col = m->col;
  res->value = (double*)malloc(sizeof(matrix_t)*res->row-res->col);

  int x = m->row;
  int y = m->col;

  for (int i = 0; i < y; i++)
  {
    if (i == 0)
    {
      for (int j = 0; j < x; j++)
      {
        v->value[j] = m->value[j*y+i];
        vs->value[j] = v->value[j];
      }

      //vector solution
      for (int j = 0; j < x; j++)
      {
        res->value[j*y+i] = vs->value[j];
      }
    }else
    {
      for (int j = 0; j < x; j++)
      {
        v->value[j] = m->value[j*y+i];
        tmp->value[j] = 0.0;
      }

      for (int j = 0; j < i; j++)
      {
        for (int k = 0; k < x; k++)
        {
          tmp->value[k] = res->value[k*y+j];
        }
        double fc = projection(v,tmp);
        for (int l = 0; l < y; l++)
        {
          v_no->value[l] = v_no->value[l] - fc*tmp->value[l];
        }
      }

      //normalize vector
      for (int k = 0; k < x; k++)
      {
        vs->value[k] = v->value[k] + v_no->value[k];
      }

      vs =norm_vector_compute(vs);

      //vector solution
      for (int k = 0; k < x; k++)
      {
        res->value[k*y+i] = vs->value[k];
      }
    }
  }
  return res;
}

//destroy allocation matrix
void destroy_alloc(matrix_t *A)
{
  free(A->value);
  free(A);
}
