#ifndef _MATRIX_H_
#define _MATRIX_H_
#include <stdio.h>
#include <stdlib.h>


//create stcucture for the matrix
typedef struct matrix_s
{
  int row;
  int col;
  int msize;
  double *value;
}matrix_t;

matrix_t *read_data(char *fname);
void print_matrix(matrix_t *m);
double dotProduct(matrix_t *m, matrix_t *n);
matrix_t *vectProduct(matrix_t *m, matrix_t *v);
double norm_vector(matrix_t *v);
double norm_frobenius(matrix_t *m);
matrix_t *add_v_matrix(matrix_t *m, matrix_t *v);
matrix_t *Horner_compute(matrix_t *m, matrix_t *v);
matrix_t *norm_vector_compute( matrix_t *m);
double projection(matrix_t *v1, matrix_t *v2);
matrix_t *gram_schmidt(matrix_t *m);
void destroy_alloc(matrix_t *A);

#endif
