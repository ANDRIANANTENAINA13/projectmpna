#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"

int main(int argc, char  **argv)
{
  if (argc < 4)
  {
    printf("Usage [bin] [matrix ] [vector ] [size of sudomain]\n");
    exit(0);
  }
  char *fname1 = argv[1];
  char *fname2 = argv[2];
  int a =atoi(argv[3]);

  matrix_t *A, *B,*res;

  A = NULL;
  B= NULL;

  A=read_data(fname1);
  B = read_data(fname2);

  if (a > A->msize)
  {
    printf("Error sub domaine > size of matrix\n");
    exit(0);
  }

  print_matrix(A);
  printf("****************************\n");
  print_matrix(B);

  res = norm_vector_compute(B);
  //compute gramschmith
  printf("Gram Schmith compute\n");
  res = gram_schmidt(A);
  print_matrix(res);

  //free allocation
  destroy_alloc(A);
  destroy_alloc(B);
  destroy_alloc(res);


  return 0;
}
